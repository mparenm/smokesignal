#!/usr/bin/env python

'''
Michael Moses
<mparenm@gmail.com>

smokesignal - Command line messaging library
'''

__version__ =   '0.2'

__all__     =   []

# dict of lists to hold signals
_signals = {
        "message"  :  [],
        "warning"  :  [],
        "error"    :  [],
        "debug"    :  [],
}

# adds text to a _signal of a specified type
def _add(text, type):
    if not _valid_text(text):
        text = str(text)
    _signals[type].append(text)

# insures the text is a string
def _valid_text(text):
    if isinstance(text, str) and len(text) > 0:
        return True
    return False

# returns the number of signals of a specified type
def _count(type):
    return len(_signals[type])

# returns the signals of a specified type, with or
# without new line padding
def _get(type, padding=False):
    signals = _signals[type]
    
    if padding is False:
        return signals
    else:
        return '\n\n'.join(signals)


class _signal(object):
    '''
    
    Interfaces with module functions, handles assigning the 
    signal type so the programs that use smokesignal don't 
    have to worry about that overhead.
    
    '''
    def __init__(self, type, immediate=False, die=False):
        '''
        Type must be in the _signals dict
        
        If immediate is True, smokesignal will will display 
        to stdout the first message of that type that is added.
        
        If die is True, smokesignal will kill the program through 
        sys.exit()
        '''
        self.type = type
        self.immediate = immediate
        self.die = die
    
    def add(self, text=None):
        '''
        Adds the text to the signal of the instance type.
        
        If text is None the message will be ignored and not added.
        '''
        error_message = "A string value needs to be passed to add a new %s" %(self.type)
        
        if text is not None:
            try:
                _add(text, self.type)
                if self.immediate is True:
                    print('\n' + self.get_last() + '\n')
                if self.die is True:
                    import sys
                    sys.exit()
            except TypeError:
                print(error_message)
    
    def get_count(self):
        return _count(self.type)
    
    def fetch(self):
        '''
        Returns the _signals array of the specified type
        '''
        return _get(self.type)
    
    def display(self):
        '''
        Prints the _signals array of the specified type to
        the stdout
        '''
        print('\n' + _get(self.type, padding=True) + '\n')
    
    def get_last(self):
        '''
        Return the last signal added of the specified type
        '''
        return _signals[self.type][len(_signals[self.type]) - 1]


# Istantiates a _signal class for signal types in the _signals dictionary.
messages     =   _signal('message')
warnings     =   _signal('warning')
errors       =   _signal('error', immediate=True, die=True)
debugs       =   _signal('debug', immediate=True)
